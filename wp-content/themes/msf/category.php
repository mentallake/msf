<?php
/**
 * Template for Category page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage MSF
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    $current_category_id = get_query_var('cat');
    $current_category_title = single_cat_title('', false);
	$current_category = get_category($current_category_id);
	$parent_category_id = $current_category->parent;
	$parent_category = false;

	if($parent_category_id > 0){
		$parent_category = get_category($parent_category_id);
	}

    $breadcrumb = get_category_parents($current_category_id, true, '!');
    $bc_array = explode('!', $breadcrumb);

    if(count($bc_array) > 0){
    	$link = new SimpleXMLElement($bc_array[0]);
		$page_title = $link[0];
    }else{
    	$page_title = $current_category_title;
    }

    $args = array(
		'posts_per_page'   => -1,
		// 'offset'           => 0,
		'category'         => $current_category_id,
		// 'category_name'    => '',
		// 'orderby'          => 'date',
		// 'order'            => 'DESC',
		// 'include'          => '',
		// 'exclude'          => '',
		// 'meta_key'         => '',
		// 'meta_value'       => '',
		// 'post_type'        => 'post',
		// 'post_mime_type'   => '',
		// 'post_parent'      => '',
		// 'author'	   		  => '',
		// 'author_name'	  => '',
		// 'post_status'      => 'publish',
		// 'suppress_filters' => true,
		// 'tag' => $year,
	);

	$cat_posts = get_posts($args);

	// Get contact us page id
	$contact_us_page = get_page_by_path( 'contact-us' );
	$contact_us_page_id = $contact_us_page->ID;
	$image_url = get_field('cover_image', $contact_us_page_id);
?>

<div id="category-page">
	<section class="cover-image-panel" style="background-image: url(<?php echo $image_url; ?>);"></section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<?php
				for($i = 0; $i < count($bc_array); $i++){
					if($bc_array[$i] == '') continue;
				?>
				<li><?php echo $bc_array[$i]; ?></li>
				<?php } ?>
			</ol>
		</div>
	</section>

	<section class="content-panel">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-title"><?php echo $page_title; ?></div>
					<div class="page-content">
						<?php
						if($parent_category){
							$parent_slug = $parent_category->slug;
 							$category = get_category_by_slug( $parent_slug );
							$get_children_cats = array(
				                'child_of' => $category->cat_ID,
				                'orderby'          => 'cat_name',
								'order'            => 'DESC',
				            );

				            $child_cats = get_categories( $get_children_cats );
						?>
						<?php // Year list for news page ?>
						<ul id="year-list" class="tag-list">
							<?php
							for($i = 0; $i < count($child_cats); $i++){
								$child_cat = $child_cats[$i];
								$child_cat_title = $child_cat->cat_name;
								$child_cat_url = get_category_link( $child_cat->term_id );
								$active_class = $current_category_title == $child_cat_title ? 'active' : '';
							?>
							<li class="<?php echo $active_class; ?>"><a href="<?php echo $child_cat_url; ?>"><?php echo $child_cat_title; ?></a></li>
							<?php } ?>
						</ul>
						<?php } ?>
						<ul class="news-list">
						<?php
						$pinned_post = array();
						$normal_post = array();

						date_default_timezone_set('Asia/Bangkok');

						foreach ($cat_posts as $post) : setup_postdata($post);
							// Check valid period
							$is_post_scheduled = get_field('is_post_scheduled');
							$is_published = false;

							if($is_post_scheduled){
								$post_start = get_field('publish_post_start');
								$post_end = get_field('publish_post_end');

								$start_datetime = date_create_from_format('d/m/Y g:i A', $post_start)->getTimestamp();
								$end_datetime = date_create_from_format('d/m/Y g:i A', $post_end)->getTimestamp();
								$current_datetime = time();

								$is_published = $current_datetime >= $start_datetime && $current_datetime <= $end_datetime;
							}else{
								$is_published = true;
							}

							if(!$is_published){
								continue;
							}

							// Check pinned period
							$is_post_pinned = get_field('is_pinned');
							$is_pinned = false;

							if($is_post_pinned){
								$pin_start = get_field('pin_schedule_start');
								$pin_end = get_field('pin_schedule_end');

								$start_datetime = date_create_from_format('d/m/Y g:i A', $pin_start)->getTimestamp();
								$end_datetime = date_create_from_format('d/m/Y g:i A', $pin_end)->getTimestamp();
								$current_datetime = time();

								$is_pinned = $current_datetime >= $start_datetime && $current_datetime <= $end_datetime;
							}

							$post->detail_url = get_permalink();

							if($is_pinned){
								$post->is_pinned = true;
								$pinned_post[] = $post;
							}else{
								$post->is_pinned = false;
								$normal_post[] = $post;
							}
						endforeach;

						$all_posts = array_merge($pinned_post, $normal_post);

						for($i = 0; $i < count($all_posts); $i++){
							$post = $all_posts[$i];
							$pin_class = $post->is_pinned ? 'pinned' : '';
							$detail_url = $post->detail_url;
							$thumbnail_url = get_the_post_thumbnail_url();
						?>
					       <li class="<?php echo $pin_class; ?>">
					            <a href="<?php echo $detail_url; ?>">
					            	<?php if($thumbnail_url != ''){ ?>
					            	<img class="post-thumbnail" src="<?php echo $thumbnail_url; ?>" alt="">
					            	<?php } ?>
					            	<?php echo $post->post_title; ?>
					            </a>
					       </li>
						<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>