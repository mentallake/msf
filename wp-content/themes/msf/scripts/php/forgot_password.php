<?php
function ajax_auth_init(){
    // Enable the user with no privileges to run ajax_forgotPassword() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxforgotpassword', 'ajax_forgotPassword' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_auth_init');
}

function ajax_forgotPassword(){
	// First check the nonce, if it fails the function will break
	check_ajax_referer('msf_forgot_password_action', 'msf_forgot_password_nonce');

	global $wpdb;

	$error = '';
	$success = '';

	$email = trim($_POST['registered_email']);

	if( isset( $_POST['action'] ) && 'ajaxforgotpassword' == $_POST['action'] ) {
		$email = trim($_POST['registered_email']);

		if( empty( $email ) ) {
			$error = 'Enter a username or e-mail address..';
		} else if( ! is_email( $email ) ) {
			$error = 'Invalid username or e-mail address.';
		} else if( ! email_exists( $email ) ) {
			$error = 'There is no user registered with that email address.';
		} else {
			$random_password = wp_generate_password( 12, false );
			$user = get_user_by( 'email', $email );

			$update_user = wp_update_user( array (
					'ID' => $user->ID,
					'user_pass' => $random_password
				)
			);

			// if update user return true then lets send user an email containing the new password
			if( $update_user ) {
				$to = $email;
				$subject = '[MSF Chula] Your new password';
				$sender = get_option('name');

				$message = 'Your new password is: ' . $random_password;

				$headers[] = 'MIME-Version: 1.0' . "\r\n";
				$headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers[] = "X-Mailer: PHP \r\n";
				$headers[] = 'From: ' . $sender . ' < ' . $email . '>' . "\r\n";

				$mail = wp_mail( $to, $subject, $message, $headers );

				if( $mail )
					$success = 'Check your email address for you new password.';

			} else {
				$error = 'Oops! something went wrong during updating your account.';
			}
		}

		$result = true;
		$message = '';

		if( ! empty( $error ) ){
			$result = false;
			$message = $error;
		}

		if( ! empty( $success ) ){
			$message = $success;
		}

		$response = array(
			"result" => $result,
			"message" => $message,
			);

		echo json_encode($response);
		exit();
	}
}
?>