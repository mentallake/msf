$(function(){
	// Initialize login form
	$('#username-textbox').attr('placeholder', 'Username');
	$('#password-textbox').attr('placeholder', 'Password');

	$('#username-textbox, #password-textbox').addClass('form-control');

	$('#login-button-panel #request-account-button').appendTo('.login-submit');

	// Initialize forgot password form
	$('#forgot-password-dialog').on('show.bs.modal', function(){
		$('#forgot-password-dialog input[type="email"]').val('');
	});

	$('#forgot-password-dialog').on('shown.bs.modal', function(){
		$('#forgot-password-dialog input[type="email"]').eq(0).focus();
	});

	$('#forgot-password-link').click(function(){
		$('#forgot-password-dialog').modal();
	});

	$('#forgot-password-form').submit(function(){
		if($('#forgot-password-textbox').val().trim() == ''){
			return false;
		}

		var options = {
			url: ajax_auth_object.ajaxurl,
            success: showForgotPasswordResponse,
            dataType: 'json'
        };

        $('#forgot-password-dialog').modal('hide');

        showLoadingPanel();

		$(this).ajaxSubmit(options);

		return false;
	});
});

function showForgotPasswordResponse(response, statusText, xhr, $form){
	if(response.result == false){
        var message = response.message;

        $('#message-dialog .modal-title').html('Forgot Password');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

        // Display message dialog
        $('#message-dialog').modal();
    }else{
    	var message = response.message;

    	$('#message-dialog .modal-title').html('Forgot Password');
        $('#message-dialog .modal-body').html(message);

        hideLoadingPanel();

        // Display message dialog
        $('#message-dialog').modal();
    }
}