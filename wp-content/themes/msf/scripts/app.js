// global var
var map = null;

$(function(){
	initMainMenus();
	initTable();
	initSlide();
	initMenuBoxes();
	initGoogleMap();

	$('select').selectpicker();

	$(window).resize();
});

function initGoogleMap() {
	$(window).on('load', function(){
		$('.acf-map').each(function(){
			// create map
			map = new_map( $(this) );
		});

		// popup is shown and map is not visible
		if($('.acf-map').length > 0){
			google.maps.event.trigger(map, 'resize');
		}
	});
}

function initMainMenus(){
	$(window).resize(function(){
		var windowWidth = $(window).width();

		if(windowWidth >= 768){
			$('#header-panel #main-menus > ul > .menu-item > a').each(function(){
				var textLength = $(this).text().length;

				if(textLength < 13){
					$(this).css('padding-top', '10px');
				}
			});
		}
	});

	$('#main-menus > ul > .menu-item.dropdown > ul').wrap('<div class="dropdown-menu-wrapper hide"></div>')

	$('#main-menus > ul > .menu-item.dropdown > a').each(function(){
		var maxMenuItemWidth = 200;
		var maxTextLength = 0;

		$(this).siblings('.dropdown-menu-wrapper').find('ul > li > a').each(function(){
			var textLength = $(this).text().length;
			maxTextLength = Math.max(maxTextLength, textLength);
		});

		maxMenuItemWidth = 30 + (maxTextLength * 8);

		var totalLvl1Menus = 0;
		$('#main-menus > ul > .menu-item.dropdown > .dropdown-menu-wrapper > ul').each(function(){
			var children = $(this).children('li').length;
			totalLvl1Menus = Math.max(totalLvl1Menus, children);
		});

		var maxHeight = totalLvl1Menus * 40 + 20;
		var windowWidth = $(window).width();

		if(windowWidth < 768){
			maxHeight = Math.min(maxHeight, 360);
		}

		$(this).menu({
	        content: $(this).next().html(),
	        showSpeed: 250,
	        width: maxMenuItemWidth,
	        height: 200,
	        maxHeight: maxHeight,
	        crumbDefaultText: ' ',
	    });
	});
}

function initTable(){
	$('table td').each(function(){
		var colWidth = $(this).outerWidth();
		$(this).attr('data-width', colWidth);
	});

	$('table').each(function(){
		var cellPadding = $(this).attr('cellpadding');

		if(cellPadding >= 0){
			$(this).find('td').css('padding', cellPadding + 'px');
		}
	});

	$(window).resize(function(){
		var windowWidth = $(window).width();

		if(windowWidth < 768){
			$('table').each(function(){
				// If there is no header, re-layout the cell on mobile device.
				if($(this).find('th').length <= 0){
					$(this).addClass('responsive');

					$(this).find('td').each(function(){
						$(this).css('width', '100%');
					});
				}
			});
		}else{
			$('table').each(function(){
				// If there is no header, re-layout the cell on mobile device.
				if($(this).find('th').length <= 0){
					$(this).removeClass('responsive');

					$(this).find('td').each(function(){
						var colWidth = $(this).attr('data-width');
						$(this).css('width', colWidth + 'px');
					});
				}
			});
		}
	});
}

function initSlide(){
	if($('#feature-image-slide .owl-carousel').length > 0){
		$('#feature-image-slide .owl-carousel').owlCarousel({
			items: 1,
		    stagePadding: 0,
		    loop: true,
		    margin: 0,
		    nav: true,
		    dots: true,
		    navText: ['<div title="Previous" class="slide-arrow medium prev">&laquo; previous</div>','<div title="Next" class="slide-arrow medium next">next &raquo; </div>'],
		    autoplay: true,
		    autoplayTimeout: 3000,
	    	autoplayHoverPause: false,
	    	animateOut: 'fadeOut',
	    	onInitialized: function(event){
	   			// var stageHeight = $(".owl-stage-outer").outerHeight();
				// $(".image-wrapper").css('height', stageHeight);
	    	}
		});
	}
}

function initMenuBoxes(){
	$(window).resize(function(){
		var windowWidth = $(window).width();

		if(windowWidth < 768){
			$('#menu-boxes-panel .menu-boxes').addClass("mobile");
		}else{
			$('#menu-boxes-panel .menu-boxes').removeClass("mobile");
		}
	});
}

function showLoadingPanel(){
    $('#loading-panel').show();
}

function hideLoadingPanel(){
    $('#loading-panel').hide();
}