<?php
/**
 * Template Name: Item list layout.
 * Template Post Type: page
 *
 * Template for News and Events page.
 * Redirect the page to the latest subcategory page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage MSF
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;
$post_id = $post->ID;
$post_slug = $post->post_name;
$post_type = get_field('post_type');
$page_title = get_the_title();

$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => $post_type,
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$all_posts = get_posts($args);

// Get all available years
$all_years = array();

foreach ($all_posts as $post) : setup_postdata($post);
	$year = get_field('year');

	if($year && !in_array($year, $all_years)){
		$all_years[] = $year;
	}
endforeach;

arsort($all_years);

$all_years = array_values($all_years);

$compare_operator = '=';
$meta_query = array();

$args = array(
	'posts_per_page'   => '-1',
	// 'offset'           => 0,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => $post_type,
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

$year_query = isset($_GET['nyear']) ? $_GET['nyear'] : false;

if($year_query){
	$current_year = $year_query;
}else{
	$current_year = count($all_years) > 0 ? $all_years[0] : false;
}

$meta_query[] = array(
	'key' => 'year',
	'value' => $current_year,
	'compare' => $compare_operator
	);

if(count($meta_query) > 0){
	$args['meta_query'] = $meta_query;
}

$cat_posts = get_posts($args);

// Get contact us page id
$contact_us_page = get_page_by_path( 'contact-us' );
$contact_us_page_id = $contact_us_page->ID;
$image_url = get_field('cover_image', $contact_us_page_id);

// Get current url
/* Get an array of Ancestors and Parents if they exist */
$parents = get_post_ancestors( $post_id );

/* Get the top Level page->ID count base 1, array base 0 so -1 */
$id = ($parents) ? $parents[count($parents)-1]: $post_id;
/* Get the parent and set the $class with the page slug (post_name) */
$parent = get_post( $id );
$parent_slug = $parent->post_name;

$full_slug = ($parent_slug && $parent_slug != $post_slug ? $parent_slug . '/' : '') . $post_slug;

$current_url = get_permalink(get_page_by_path($full_slug));
?>

<div id="research-series-page">
	<section class="cover-image-panel" style="background-image: url(<?php echo $image_url; ?>);"></section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="<?php echo $current_url; ?>"><?php echo $page_title; ?></a></li>
			</ol>
		</div>
	</section>

	<section class="content-panel">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-title"><?php echo $page_title; ?></div>
					<div class="page-content">
						<?php // Year list for news page ?>
						<ul id="year-list" class="tag-list">
							<?php
							for($i = 0; $i < count($all_years); $i++){
								$year = $all_years[$i];
								$year_search_url = $current_url . '?nyear=' . $year;

								$active_class = $current_year == $year ? 'active' : '';
							?>
							<li class="<?php echo $active_class; ?>"><a href="<?php echo $year_search_url; ?>"><?php echo $year; ?></a></li>
							<?php } ?>
						</ul>
						<ul class="news-list">
						<?php
						$pinned_post = array();
						$normal_post = array();

						date_default_timezone_set('Asia/Bangkok');

						foreach ($cat_posts as $post) : setup_postdata($post);
							// Check valid period
							$is_post_scheduled = get_field('is_post_scheduled');
							$is_published = false;

							if($is_post_scheduled){
								$post_start = get_field('publish_post_start');
								$post_end = get_field('publish_post_end');

								$start_datetime = date_create_from_format('d/m/Y g:i A', $post_start)->getTimestamp();
								$end_datetime = date_create_from_format('d/m/Y g:i A', $post_end)->getTimestamp();
								$current_datetime = time();

								$is_published = $current_datetime >= $start_datetime && $current_datetime <= $end_datetime;
							}else{
								$is_published = true;
							}

							if(!$is_published){
								continue;
							}

							// Check pinned period
							$is_post_pinned = get_field('is_pinned');
							$is_pinned = false;

							if($is_post_pinned){
								$pin_start = get_field('pin_schedule_start');
								$pin_end = get_field('pin_schedule_end');

								$start_datetime = date_create_from_format('d/m/Y g:i A', $pin_start)->getTimestamp();
								$end_datetime = date_create_from_format('d/m/Y g:i A', $pin_end)->getTimestamp();
								$current_datetime = time();

								$is_pinned = $current_datetime >= $start_datetime && $current_datetime <= $end_datetime;
							}

							$post->detail_url = get_permalink();

							if($is_pinned){
								$post->is_pinned = true;
								$pinned_post[] = $post;
							}else{
								$post->is_pinned = false;
								$normal_post[] = $post;
							}
						endforeach;

						$all_posts = array_merge($pinned_post, $normal_post);

						for($i = 0; $i < count($all_posts); $i++){
							$post = $all_posts[$i];
							$pin_class = $post->is_pinned ? 'pinned' : '';
							$detail_url = $post->detail_url;
							$thumbnail_url = get_the_post_thumbnail_url();
						?>
					       <li class="<?php echo $pin_class; ?>">
					            <a href="<?php echo $detail_url; ?>">
					            	<?php if($thumbnail_url != ''){ ?>
					            	<div class="post-thumbnail-wrapper" style1="background-image: url('<?php echo $thumbnail_url; ?>');">
					            		<img src="<?php echo $thumbnail_url; ?>" alt="">
					            	</div>
					            	<?php } ?>
					            	<?php echo $post->post_title; ?>
					            </a>
					       </li>
						<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>