<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Get contact us page id
	$contact_us_page = get_page_by_path( 'contact-us' );
	$contact_us_page_id = $contact_us_page->ID;
?>

<div id="home-page">
	<section id="feature-image-slide">
		<?php if( have_rows('slide') ): ?>
		<ul class="slides owl-carousel owl-theme">
			<?php
			while( have_rows('slide') ): the_row();
				// vars
				$image = get_sub_field('image');
				$url_type = get_sub_field('url_type');
				$link = "";
				$is_external = false;
				$post_start = get_sub_field('publish_post_start');
				$post_end = get_sub_field('publish_post_end');

				$start_datetime = date_create_from_format('d/m/Y g:i A', $post_start)->getTimestamp();
				$end_datetime = date_create_from_format('d/m/Y g:i A', $post_end)->getTimestamp();
				$current_datetime = time();

				$is_published = $current_datetime >= $start_datetime && $current_datetime <= $end_datetime;

				if(!$is_published || $image == ''){
					continue;
				}

				if($url_type == 'Internal'){
					$link = get_sub_field('page_link');
				}elseif($url_type == 'External'){
					$link = get_sub_field('url');
					$is_external = true;
				}
			?>
			<li class="item">
				<?php if( $link ): ?>
				<a href="<?php echo $link; ?>" <?php echo $is_external ? 'target="_blank"' : ''; ?>>
				<?php endif; ?>
					<!-- <div class="image-wrapper" style="background-image: url('<?php echo $image['url']; ?>');"> -->
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					<!-- </div> -->
				<?php if( $link ): ?>
				</a>
				<?php endif; ?>
			</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>
	</section>

	<section id="menu-boxes-panel" class="content-panel">
		<div class="container">
			<?php
			$rows = get_field('menu_boxes');
			$total_boxes = count($rows);
			$width = floatval(100 / $total_boxes);
			$width = $width . '%';

			if( have_rows('menu_boxes') ):
			?>
			<ul class="menu-boxes">
				<?php
				while( have_rows('menu_boxes') ): the_row();
					// vars
					$title = get_sub_field('box_title');
					$content = get_sub_field('box_content');
					$link = get_sub_field('page_link');
				?>
				<li class="menu-box" style="width: <?php echo $width; ?>;">
					<a href="<?php echo $link; ?>">
						<div class="box-title"><?php echo $title; ?></div>
						<div class="box-content"><?php echo $content; ?></div>
					</a>
				</li>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</section>

	<section id="feature-contents" class="content-color-band content-panel">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="section-title"><?php the_field('home_video_title'); ?></div>
					<div class="">
						<?php the_field('home_video_script'); ?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="section-title"><?php the_field('home_middle_title_2'); ?></div>
					<div class="">
						<?php the_field('home_middle_content_2'); ?>
					</div>
					<?php
					$read_more_url = get_field('home_middle_content_2_read_more');

					if($read_more_url != ''){
					?>
					<br>
					<a href="<?php echo $read_more_url; ?>" class="btn btn-primary">Read more</a>
					<?php } ?>
				</div>
				<div class="col-sm-4">
					<div class="section-title"><?php the_field('home_middle_title_3'); ?></div>
					<div class="">
						<?php the_field('home_middle_content_3'); ?>
					</div>
					<?php
					$read_more_url = get_field('home_middle_content_3_read_more');

					if($read_more_url != ''){
					?>
					<br>
					<a href="<?php echo $read_more_url; ?>" class="btn btn-primary">Read more</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>

	<section id="contact-panel" class="content-panel">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<div class="section-title"><?php the_field('navigation_title'); ?></div>
					<div class="navigation-content">
						<?php the_field('navigation_content'); ?>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="section-title"><?php the_field('chula_contact_title'); ?></div>
					<div class="row narrow">
						<div class="col-xs-4">
							<?php $image_url = get_field('chula_contact_image'); ?>
							<img src="<?php echo $image_url; ?>" alt="">
						</div>
						<div class="col-xs-8">
							<?php the_field('address', $contact_us_page_id); ?><br>
							Tel: <?php the_field('phone', $contact_us_page_id); ?><br>
							Fax: <?php the_field('fax', $contact_us_page_id); ?><br>
							Email: <?php the_field('email', $contact_us_page_id); ?><br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer();