<?php
/**
 * Template for single page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    global $page;
    $post_slug = $page->post_name;

    // Get contact us page id
	$contact_us_page = get_page_by_path( 'contact-us' );
	$contact_us_page_id = $contact_us_page->ID;

	$id = $page->id;
	$post = get_post($id);
	$content = apply_filters('the_content', $post->post_content);
	$categories = get_the_category($post->ID);
	$category_name = $categories[0]->name;
	$category_slug = $categories[0]->slug;
	$last_modified_date = $post->post_modified;
	$post_type_slug = get_post_type($post->ID);

	if($post_type_slug == 'thesis'){
		$parent_slug = "thesis-database";
		$parent_title = "Thesis Database";
	}elseif($post_type_slug == 'testimonial'){
		$parent_slug = "our-alumni/testimonial";
		$parent_title = "Testimonial";
	}elseif($post_type_slug == 'news'){
		$parent_slug = "news-and-announcements";
		$parent_title = "News And Announcements";
	}elseif($post_type_slug == 'research_series'){
		$parent_slug = "faculty-and-research/research-series";
		$parent_title = "Research Series";
	}
?>

<div id="page">
	<?php
	$image_url = get_field('cover_image');

	if($image_url == ''){
		$image_url = get_field('cover_image', $contact_us_page_id);
	}
	?>
	<section class="cover-image-panel" style="background-image: url(<?php echo $image_url; ?>);"></section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<?php if(isset($parent_title)){ ?>
				<li><a href="<?php echo get_permalink(get_page_by_path($parent_slug)); ?>"><?php echo $parent_title; ?></a></li>
				<?php } ?>
				<li><a href="<?php echo get_permalink(get_page_by_path($post_slug)); ?>"><?php the_title(); ?></a></li>
			</ol>
		</div>
	</section>

	<section class="content-panel">
		<div class="container">
			<div class="page-title"><?php the_title(); ?></div>
			<div class="post-by">
				<?php
				$post_date = get_the_date( 'l, j F Y h:i A' );
				echo "Post by MSF Chula at " . $post_date;
				?>
			</div>
			<div class="page-content">
				<?php //echo $content; ?>
				<div class="row">
				<?php
				// check if the flexible content field has rows of data
				if( have_rows('layout') ):
				     // loop through the rows of data
				    while ( have_rows('layout') ) : the_row();
				        if( get_row_layout() == 'multicolumn' ):
				        	$columns = get_sub_field('columns');
				        	$col_contents = array();

				        	$col_contents[] = get_sub_field('column_content_1');
				        	$col_contents[] = get_sub_field('column_content_2');
				        	$col_contents[] = get_sub_field('column_content_3');

				        	if($columns == 1){
				        		$col_class = "col-sm-12";
				        	}elseif($columns == 2){
				        		$col_class = "col-sm-6";
				        	}if($columns == 3){
				        		$col_class = "col-sm-4";
				        	}

				        	for($i = 0; $i < $columns; $i++){ ?>
				        	<div class="<?php echo $col_class; ?>">
								<?php echo $col_contents[$i]; ?>
							</div>
							<?php }
						else:

				        endif;
				    endwhile;
				else : ?>
					<div class="col-xs-12">
						<?php echo wpautop($post->post_content); ?>
					</div>
				    <?php
				endif;
				?>
				</div>
				<?php
				$modified_datetime = get_the_modified_time('l, j F Y h:i A');

				$full_modified_datetime = "Last updated at " . $modified_datetime;
				?>
				<div class="last-update-panel"><?php echo $full_modified_datetime; ?></div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>