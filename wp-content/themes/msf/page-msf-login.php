<?php
/**
 * Template for Student Login page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
global $post;
$post_slug = $post->post_name;
?>

<div id="msf-login-page" class="animated fadeIn">
	<section id="login-panel" class="content-panel">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-5 col-sm-6 left-col">
					<img id="cbs-logo" src="<?php echo get_template_directory_uri() . '/images/cbs-logo.png'; ?>" alt="">
				</div>
				<div class="col-xs-7 col-sm-6 right-col">
					<div class="form-wrapper">
						<div id="login-inner-form">
							<div id="login-title">Master of Science in Finance</div>
							<div id="login-subtitle">User Logon Form</div>

							<?php
							$args = array(
								'form_id' => 'student-login-form',
								'id_username' => 'username-textbox',
								'id_password' => 'password-textbox',
								'label_username' => __( '' ),
								'label_password' => __( '' ),
								'label_remember' => __( 'Sign me in automatically' ),
								);
							wp_login_form($args);
							?>
							<p id="login-button-panel" class="hide">
								<button id="request-account-button" type="button" tabindex="6" class="hide">Request Account</button>
							</p>
							<p class="comment" style="height: 0"></p>
							<p>
								<a id="forgot-password-link" title="Forgot password" class="">Forgot password?</a>
							</p>

							<?php
							$login = (isset($_GET['login']) ) ? $_GET['login'] : 0;

							if ( $login === "failed" ) {
								echo '<p class="login-msg">Invalid username and/or password.</p>';
							} elseif ( $login === "empty" ) {
								echo '<p class="login-msg">Username and/or Password is empty.</p>';
							} elseif ( $login === "false" ) {
								echo '<p class="login-msg">You are logged out.</p>';
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>

<div id="forgot-password-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center" role="document">
			<div class="modal-content">
				<form id="forgot-password-form" method="post">
					<input type="hidden" name="action" value="ajaxforgotpassword">
					<?php
					// this prevent automated script for unwanted spam
					if ( function_exists( 'wp_nonce_field' ) )
						wp_nonce_field( 'msf_forgot_password_action', 'msf_forgot_password_nonce' );
					?>

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Forgot Password</h4>
					</div>
					<div class="modal-body">
						<p>
							Please specify your register email address.
						</p>
						<div class="form-group">
							<input type="email" id="forgot-password-textbox" name="registered_email" class="form-control" placeholder="Registered Email" required>
						</div>
						<p>
							* The new password will be sent to your registered email.
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default size-1" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary size-1">Confirm</button>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->

<div id="message-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Message</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary size-1" data-dismiss="modal">OK</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
</div><!-- /.modal -->

<div id="loading-panel">
    <img class="" src="<?php echo get_template_directory_uri() . '/images/loading.gif'; ?>">
</div>