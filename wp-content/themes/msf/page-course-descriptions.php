<?php
/**
 * Template for Course Description page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    global $post;
    $post_slug = $post->post_name;
?>

<div id="course-descriptions-page">
	<section class="cover-image-panel" style="background-image: url(<?php the_field('cover_image'); ?>);">
	</section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="<?php echo get_permalink(get_page_by_path($post_slug)); ?>"><?php the_title(); ?></a></li>
			</ol>
		</div>
	</section>

	<section class="content-panel">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-title"><?php the_title(); ?></div>
					<div class="page-content">
						<?php
						$id = $page->id;
						$post = get_post($id);
						$content = apply_filters('the_content', $post->post_content);

						echo $content;
						?>

						<?php if( have_rows('course_description') ): ?>
						<div id="course-description-table-wrapper">
							<table>
								<thead>
									<tr>
										<th width="150" class="text-center">Course No.</th>
										<th class="text-center">Course Description</th>
										<th width="150" class="text-center">Credit</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count = 0;

									while( have_rows('course_description') ): the_row();
										$course_no = get_sub_field('course_no');
										$course_title = get_sub_field('course_title');
										$course_desc = get_sub_field('course_description');
										$course_credit = get_sub_field('credit');
										$count++;
									?>
									<tr>
										<td class="text-center"><?php echo $course_no; ?></td>
										<td class="">

											<a class="collapsed" role="button" data-toggle="collapse" href="#course-description-<?php echo $count; ?>" aria-expanded="false" aria-controls="collapseExample">
												<?php echo $course_title; ?>
											</a>
											<div class="collapse" id="course-description-<?php echo $count; ?>">
											  	<?php echo $course_desc; ?>
											</div>
										</td>
										<td class="text-center"><?php echo $course_credit; ?></td>
									</tr>
									<?php endwhile; ?>
								</tbody>
							</table>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>