<?php
/**
 * Template for Contact us page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    global $post;
    $post_slug = $post->post_name;
?>

<div id="contact-us-page">
	<section class="cover-image-panel" style="background-image: url(<?php the_field('cover_image'); ?>);">
	</section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="<?php echo get_permalink(get_page_by_path($post_slug)); ?>"><?php the_title(); ?></a></li>
			</ol>
		</div>
	</section>

	<section class="content-panel">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-title"><?php the_title(); ?></div>
					<div class="map-panel">
					<?php
					$map = get_field('map');

					if( !empty($map) ):
					?>
					<div class="acf-map">
						<div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
					</div>
					<?php endif; ?>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="content-col">
								<div class="col-title">Address:</div>
								<div class="contact-item address">
									<?php the_field('address'); ?>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="content-col">
								<div class="col-title">Phone:</div>
								<ul class="contact-item-list">
									<li class="contact-item phone">
										<a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a>
									</li>
									<li class="contact-item fax">
										<a href="tel:<?php the_field('fax'); ?>"><?php the_field('fax'); ?></a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="content-col">
								<div class="col-title">Email:</div>
								<ul class="contact-item-list">
									<li class="contact-item email">
										<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>