<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage MSF
 * @since 1.0
 */

get_header(); ?>

<?php
// Get contact us page id
$contact_us_page = get_page_by_path( 'contact-us' );
$contact_us_page_id = $contact_us_page->ID;
$image_url = get_field('cover_image', $contact_us_page_id);
?>

<div id="404-page">
	<section class="cover-image-panel" style="background-image: url(<?php echo $image_url; ?>);"></section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="<?php echo get_permalink(get_page_by_path($post_slug)); ?>"><?php the_title(); ?></a></li>
			</ol>
		</div>
	</section>

	<br>

	<section class="content-panel error-404 not-found text-center">
		<div class="container">
			<div class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'MSF' ); ?></div>

			<div class="page-content">
				<p><?php _e( 'It looks like nothing was found at this location.', 'MSF' ); ?> Back to <a href="<?php echo home_url(); ?>">Home Page</a></p>

				<?php // get_search_form(); ?>
			</div><!-- .page-content -->
		</div>
	</section><!-- .error-404 -->

	<br>
</div>

<?php get_footer(); ?>