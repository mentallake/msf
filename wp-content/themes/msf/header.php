<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title(); ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() . '/images/favicon.ico'; ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>
    </head>

    <?php
    // Get contact us page id
	$contact_us_page = get_page_by_path( 'contact-us' );
	$contact_us_page_id = $contact_us_page->ID;
    ?>

	<body <?php body_class(); ?>>
		<header id="header-panel">
			<nav class="navbar">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menus" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo home_url(); ?>"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div id="header-top-panel" class="text-right hidden-xs hidden-sm">
					<?php
					$page = get_page_by_path( 'msf-login' );
					$msf_login_id = $page->ID;
					$msf_login_url = get_permalink( $msf_login_id );
					?>
						<ul id="header-top-menu-list">
							<li><a href="<?php echo home_url(); ?>">Home</a></li>
							<?php if (is_user_logged_in()) : ?>
							<li id="account-menu" class="dropdown">
								<?php
								$current_user = wp_get_current_user();
								$username = $current_user->user_login;
								?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $username; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo wp_logout_url(get_permalink()); ?>">Logout</a></li>
								</ul>
							</li>
							<?php else : ?>
							<li><a href="<?php echo $msf_login_url; ?>">Login</a></li>
							<?php endif;?>
						</ul>

						<div id="header-contact" class="">
							<img src="<?php echo get_template_directory_uri() . '/images/icon-phone.png'; ?>" alt="">
							<div id="header-contact-content">
								<div id="phone"><?php echo str_replace('+66 ', '0', get_field('phone', $contact_us_page_id)); ?></div>
								<div id="msf-text">​MSF PROGRAM OFFICE</div>
							</div>
						</div>
					</div>

					<?php
					wp_nav_menu( array(
		                'depth'             => 4,
		                'menu_class'        => 'nav navbar-nav',
		                'container'         => 'div',
		                'container_class'   => 'collapse navbar-collapse',
		                'container_id'      => 'main-menus',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
					?>
				</div><!-- /.container-fluid -->
			</nav>
		</header>

		<div id="page" class="hfeed site">
			<div id="sidebar" class="sidebar hide">
				<header id="masthead" class="site-header" role="banner">
					<div class="site-branding">
						<?php
							if ( is_front_page() && is_home() ) : ?>
								<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php else : ?>
								<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
							<?php endif;

							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
								<p class="site-description"><?php echo $description; ?></p>
							<?php endif;
						?>
						<button class="secondary-toggle"><?php _e( 'Menu and widgets', 'msf' ); ?></button>
					</div><!-- .site-branding -->
				</header><!-- .site-header -->

				<?php get_sidebar(); ?>
			</div><!-- .sidebar -->

			<div id="content" class="site-content">
