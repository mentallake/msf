<?php
/**
 * Template for Category page.
 *
 * @link
 *
 * @package WordPress
 * @subpackage MSF
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
global $post;
$post_slug = $post->post_name;
$page_title = get_the_title();

// $category = get_category_by_slug( 'thesis' );
// $current_category_id = $category->cat_ID;

$posts_per_page = 15;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$offset = ($paged - 1) * $posts_per_page;

$text_query = isset($_GET['text']) ? $_GET['text'] : false;
$author_query = isset($_GET['author']) ? $_GET['author'] : false;
$advisor_query = isset($_GET['advisor']) ? $_GET['advisor'] : false;
$type_query = isset($_GET['type']) ? $_GET['type'] : false;
$year_query = isset($_GET['tyear']) ? $_GET['tyear'] : false;

$compare_operator = '=';
$is_text_search = false;
$text_query = trim($text_query) != '' ? $text_query : false;

$meta_query = array();

if($author_query){
	$meta_query[] = array(
		'key' => 'author',
		'value' => $author_query,
		'compare' => $compare_operator
		);
}

if($advisor_query){
	$meta_query[] = array(
		'key' => 'advisor',
		'value' => $advisor_query,
		'compare' => $compare_operator
		);
}

if($type_query){
	$type_query = $type_query == '-' ? '' : $type_query;

	$meta_query[] = array(
		'key' => 'type',
		'value' => $type_query,
		'compare' => $compare_operator
		);
}

$selected_year = false;

if($year_query && $year_query != "All"){
	$meta_query[] = array(
		'key' => 'year',
		'value' => $year_query,
		'compare' => $compare_operator
		);

	$selected_year = $year_query;
}

if(count($meta_query) > 1){
	$meta_query['relation'] = 'AND';
}

$args = array(
	'posts_per_page'   => '-1',
	'offset'           => $offset,
	// 'category'         => $current_category_id,
	// 'category_name'    => '',
	// 'orderby'          => 'date',
	// 'order'            => 'DESC',
	// 'include'          => '',
	// 'exclude'          => '',
	// 'meta_key'         => '',
	// 'meta_value'       => '',
	'post_type'        => 'thesis',
	// 'post_mime_type'   => '',
	// 'post_parent'      => '',
	// 'author'	   		  => '',
	// 'author_name'	  => '',
	// 'post_status'      => 'publish',
	// 'suppress_filters' => true,
	// 'tag' => $year,
);

if($text_query){
	if(count($meta_query) > 0){
		$args['_meta_with_title'] = $text_query;
	}else{
		$args['_meta_or_title'] = $text_query;
	}

	$compare_operator = 'LIKE';

	$more_query = array();
	$more_query['relation'] = 'OR';

	$more_query[] = array('key' => 'post_title',
						'value' => $text_query,
						'compare' => $compare_operator
						);

	// If user search by keyword then makes a search in every fields.
	if(!$author_query){
		$more_query[] = array('key' => 'author',
							'value' => $text_query,
							'compare' => $compare_operator
							);
	}

	if(!$advisor_query){
		$more_query[] = array('key' => 'advisor',
							'value' => $text_query,
							'compare' => $compare_operator
							);
	}

	if(!$type_query){
		$more_query[] = array('key' => 'type',
							'value' => $text_query,
							'compare' => $compare_operator
							);
	}

	$meta_query[] = $more_query;
}

if(count($meta_query) > 0){
	$args['meta_query'] = $meta_query;
}

$all_cat_posts = get_posts($args);

// Get number of all related posts.
$all_cat_posts_count = count($all_cat_posts);
$cat_posts = array_slice($all_cat_posts, 0, $posts_per_page);

// Get number of all posts.
$args = array(
	'posts_per_page'   => -1,
	'post_type'        => 'thesis',
);

$all_posts = get_posts($args);
$all_posts_count = count($all_posts);

// Get all available years
$all_years = array("All");

foreach ($all_posts as $post) : setup_postdata($post);
	$year = get_field('year');

	if(!in_array($year, $all_years)){
		$all_years[] = $year;
	}
endforeach;

arsort($all_years);

$all_years = array_values($all_years);

// Get contact us page id
$contact_us_page = get_page_by_path( 'contact-us' );
$contact_us_page_id = $contact_us_page->ID;
$image_url = get_field('cover_image', $contact_us_page_id);

// Get current url
$current_url = get_permalink(get_page_by_path('thesis-database'));
?>

<div id="thesis-database-page">
	<section class="cover-image-panel" style="background-image: url(<?php echo $image_url; ?>);"></section>

	<section class="breadcrumb-panel">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="<?php echo get_permalink(get_page_by_path($post_slug)); ?>"><?php echo $page_title; ?></a></li>
			</ol>
		</div>
	</section>

	<section class="content-panel">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page-title"><?php echo $page_title; ?></div>
					<div class="page-content">
						<div class="row">
							<div class="col-sm-offset-6 col-sm-6">
								<div class="input-group">
									<span class="input-group-btn">
										<select name="" id="search-year-select" data-style="btn-primary" data-width="100px" data-size="7">
											<?php for($i = 0; $i < count($all_years); $i++){ ?>
											<option value="<?php echo $all_years[$i]; ?>" <?php echo ($selected_year == $all_years[$i]) ? 'selected' : ''; ?>><?php echo $all_years[$i]; ?></option>
											<?php } ?>
										</select>
									</span>
									<input type="text" id="search-thesis-textbox" class="form-control" placeholder="Enter Keyword" value="<?php echo $text_query; ?>">
									<span class="input-group-btn">
										<a id="search-thesis-btn" class="btn btn-primary" href="<?php echo $current_url; ?>"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</a>
									</span>
								</div>
							</div>
						</div>
						<table class="table">
							<thead>
								<tr>
									<th class="text-center">Title</th>
									<th class="text-center">Author</th>
									<th class="text-center">Advisor</th>
									<th class="text-center">Type</th>
									<th width="100px" class="text-center">Year</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach ($cat_posts as $post) : setup_postdata($post);
								$title = $post->post_title;
								$author = get_field('author');
								$advisor = get_field('advisor');
								$type = get_field('type');
								$type = trim($type) == '' ? '-' : $type;
								$year = get_field('year');
								$detail_url = get_permalink();
								$author_search_url = $current_url . '?author=' . $author;
								$advisor_search_url = $current_url . '?advisor=' . $advisor;
								$type_search_url = $current_url . '?type=' . $type;
								$year_search_url = $current_url . '?tyear=' . $year;
							?>
								<tr>
									<td class=""><a href="<?php echo $detail_url; ?>"><?php echo $title; ?></a></td>
									<td class="text-center"><a href="<?php echo $author_search_url; ?>"><?php echo $author; ?></a></td>
									<td class="text-center"><a href="<?php echo $advisor_search_url; ?>"><?php echo $advisor; ?></a></td>
									<td class="text-center"><a href="<?php echo $type_search_url; ?>"><?php echo $type; ?></a></td>
									<td class="text-center"><a href="<?php echo $year_search_url; ?>"><?php echo $year; ?></a></td>
								</tr>
							<?php endforeach; ?>

							<?php if(count($cat_posts) <= 0){ ?>
								<tr class="empty-row">
									<td class="text-center" colspan="5">No data to be displayed</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
						<div class="page-numbers-wrapper text-center">
							<?php
							$total_pages = $all_cat_posts_count / $posts_per_page;
							$total_pages += $all_cat_posts_count % $posts_per_page > 0 ? 1 : 0;
							$current_page = ($offset / $posts_per_page) + 1;
							$big = 999999999; // need an unlikely integer

							$args = array(
								'base'               => $current_url . 'page/%#%',
								'format'             => '?paged=%#%',
								'total'              => $total_pages,
								'current'            => $current_page,
								'show_all'           => false,
								'end_size'           => 1,
								'mid_size'           => 2,
								'prev_next'          => true,
								'prev_text'          => __('« Previous'),
								'next_text'          => __('Next »'),
								'type'               => 'list',
								'add_args'           => false,
								'add_fragment'       => '',
								'before_page_number' => '',
								'after_page_number'  => ''
							);

							echo paginate_links($args);
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php get_footer(); ?>

<script>
	$(document).ready(function(){
		$('#search-thesis-textbox').keypress(function(e){
			var key = e.which;

			if(key == 13){ // the enter key code
			    $('#search-thesis-btn').click();
			    return false;
			}
		});

		$('#search-thesis-btn').click(function(){
			var currentUrl = $(this).attr('href');
			var year = $('#search-year-select').selectpicker('val');
			var text = $('#search-thesis-textbox').val();
			var searchUrl = currentUrl + '?tyear=' + year + '&text=' + text;

			window.location.href = searchUrl;

			return false;
		});
	});
</script>