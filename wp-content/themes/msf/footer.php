<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link
 *
 * @package WordPress
 * @subpackage msf
 * @since 1.0
 * @version 1.0
 */

?>
		<?php
		$page = get_page_by_path( 'contact-us' );
		$contact_us_page_id = $page->ID;
		?>

		<footer id="footer-panel">
			<div class="container">
				<ul id="footer-image-list">
					<li><img src="<?php echo get_template_directory_uri() . '/images/cbs-logo.jpg'; ?>" alt=""></li>
					<li><img src="<?php echo get_template_directory_uri() . '/images/AACSB.gif'; ?>" alt=""></li>
					<li><img src="<?php echo get_template_directory_uri() . '/images/EPAS.gif'; ?>" alt=""></li>
				</ul>
			</div>

			<div id="footer-bottom-panel">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 footer-bottom-left">
							The Master of Science in Finance · Copyright © <?php echo date("Y"); ?> · All Rights Reserved
						</div>
						<div class="col-sm-4 footer-bottom-right">
							<span>Follow me</span>&nbsp;&nbsp;
							<ul id="footer-social-list">
								<?php if(get_field('facebook', $contact_us_page_id) != ''){ ?>
								<li class="social-item facebook"><a href="<?php the_field('facebook', $contact_us_page_id); ?>" target="_blank"></a></li>
								<?php } ?>
								<?php if(get_field('twitter', $contact_us_page_id) != ''){ ?>
								<li class="social-item twitter"><a href="<?php the_field('twitter', $contact_us_page_id); ?>" target="_blank"></a></li>
								<?php } ?>
								<?php if(get_field('google_plus', $contact_us_page_id) != ''){ ?>
								<li class="social-item google-plus"><a href="<?php the_field('google_plus', $contact_us_page_id); ?>" target="_blank"></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<?php wp_footer(); ?>
	</body>
</html>