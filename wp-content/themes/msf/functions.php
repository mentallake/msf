<?php
require_once('libs/wp_bootstrap_navwalker.php');
require_once( get_template_directory() . '/scripts/php/forgot_password.php' );

define('MSF_LOGIN_SLUG', "msf-login");

function add_scripts(){
	// Css
    wp_enqueue_style('animate-css', get_template_directory_uri() . '/css/animate.css');
    wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/fonts/font-awesome-4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/libs/bootstrap-3.3.7-custom/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-select-css', get_template_directory_uri() . '/libs/bootstrap-select-1.12.2/css/bootstrap-select.min.css');
	wp_enqueue_style('wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500', false );
    wp_enqueue_style('fg-menu-css', get_template_directory_uri() . '/libs/fg-menu/fg.menu.css');
    wp_enqueue_style('fg-menu-theme-css', get_template_directory_uri() . '/libs/fg-menu/theme/ui.all.css');
    wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/libs/owlCarousel2-2.2.0/assets/owl.carousel.min.css');
    wp_enqueue_style('owl-carousel-theme-css', get_template_directory_uri() . '/libs/owlCarousel2-2.2.0/assets/owl.theme.default.min.css');
	wp_enqueue_style('msf-style', get_stylesheet_uri());

	// Libs
	wp_enqueue_script('modernizr-js', get_template_directory_uri() . '/libs/modernizr-2.8.3.min.js');
	wp_enqueue_script('jquery-js', get_template_directory_uri() . '/libs/jquery-3.1.1.min.js');
	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/libs/bootstrap-3.3.7-custom/js/bootstrap.min.js');
    wp_enqueue_script('bootstrap-select-js', get_template_directory_uri() . '/libs/bootstrap-select-1.12.2/js/bootstrap-select.min.js');
    wp_enqueue_script('fg-menu-js', get_template_directory_uri() . '/libs/fg-menu/fg.menu.js');
    wp_enqueue_script('owl-carousel-js', get_template_directory_uri() . '/libs/owlCarousel2-2.2.0/owl.carousel.min.js');
    wp_enqueue_script('google-map-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCl67f5wvfat5T8RtEc14i8UWZ1FCjYqrY');
    wp_enqueue_script('jquery-form-js', get_template_directory_uri() . '/libs/jquery.form.min.js');

	// Scripts
    wp_enqueue_script('google-map-helper-js', get_template_directory_uri() . '/scripts/google-map-helper.js');
	wp_enqueue_script('app-js', get_template_directory_uri() . '/scripts/app.js');

    if(is_page(MSF_LOGIN_SLUG)){
        wp_enqueue_script('login-js', get_template_directory_uri() . '/scripts/login.js', array(), false, true);

        wp_localize_script( 'login-js', 'ajax_auth_object', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'redirecturl' => home_url(),
            'loadingmessage' => __('Sending user info, please wait...')
        ));
    }
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );

// Hide admin bar if user is student
$current_user = wp_get_current_user();
$role_name = '';

if(count($current_user->roles) > 0){
    $role_name = $current_user->roles[0];
}

if ( 'student' === $role_name ) {
    show_admin_bar( false );
} // if $role_name

// Add menus to WP Admin sidebar
add_theme_support( 'menus' );

// Add google map api key for ACF
function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyCl67f5wvfat5T8RtEc14i8UWZ1FCjYqrY';

	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// Make WP Content Editor to be positioned in ACF Field.
function my_acf_admin_head() {
    ?>
    <script type="text/javascript">
    (function($) {
        $(document).ready(function(){
            // For general page, to move editor in 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#postdivrich').length > 0){
                $('.acf-field-message.container-message').append( $('#postdivrich') );
            }

            // For course description page, to move table in 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_58d5041ec6f45').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_58d5041ec6f45') );
            }

            // Page Layout: Move to 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_58fcbdf4ac01b').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_58fcbdf4ac01b') );
            }

            // Content Member Area: Move to 'content' tab.
            // if($('.acf-field-message.container-message').length > 0 &&
            //    $('#acf-group_5904d38c4b80e').length > 0){
            //     $('.acf-field-message.container-message').append( $('#acf-group_5904d38c4b80e') );
            // }

            // Content Permission: Move to 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#members-cp').length > 0){
                $('.acf-field-message.container-message').append( $('#members-cp') );
            }

            // Thesis post: Move to 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_5903853903011').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_5903853903011') );
            }

            // News Year: Move to 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_5989728338952').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_5989728338952') );
            }

            // Item fields: Move to 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_59a163ff6b2be').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_59a163ff6b2be') );
            }

            // Local
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_59a16515d79c9').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_59a16515d79c9') );
            }

            // Schedule Setting: Move to 'content' tab.
            if($('.acf-field-message.container-message').length > 0 &&
               $('#acf-group_5902f7af1b86f').length > 0){
                $('.acf-field-message.container-message').append( $('#acf-group_5902f7af1b86f') );
            }

            $('body').scrollTop(10);
        });
    })(jQuery);
    </script>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

// Set default font to Tinymce
function wpse8171_add_editor_styles() {
    add_editor_style( 'editor.css' );
}

add_action( 'init', 'wpse8171_add_editor_styles' );

// Set title
function set_title($data){
    global $post;
    // where $data would be string(#) "current title"
    // Example:
    // (you would want to change $post->ID to however you are getting the book order #,
    // but you can see how it works this way with global $post;)

    if(is_category()){
        $current_category_id = get_query_var('cat');
        $current_category_title = single_cat_title('', false);

        $breadcrumb = get_category_parents($current_category_id, true, '!');
        $bc_array = explode('!', $breadcrumb);

        if(count($bc_array) > 0){
            $link = new SimpleXMLElement($bc_array[0]);
            $page_title = $link[0];
        }else{
            $page_title = $current_category_title;
        }
        return $page_title . ' : The Master of Science in Finance';
    }else{
        return $post->post_title . ' : The Master of Science in Finance';
    }
}

add_filter('wp_title','set_title');

// Enable post thumbnail
add_theme_support( 'post-thumbnails' );

// Change login logo
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/cbs-logo.png);
            height: 80px;
            width: 100%;
            background-position: center center;
            background-size: auto 100%;
            background-repeat: no-repeat;
            padding-bottom: 30px;
        }
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Redirect wp-login page to msf login page
function redirect_login_page() {
    $login_page  = home_url( MSF_LOGIN_SLUG );
    $page_viewed = basename($_SERVER['REQUEST_URI']);
    $is_wp_login = strpos($page_viewed, "wp-login.php") !== false;
    $is_wp_forgot_password = strpos($page_viewed, "wp-login.php?action=lostpassword") !== false;
    $is_msf_login = strpos($page_viewed, MSF_LOGIN_SLUG) !== false;

    if( ! is_user_logged_in() && $is_wp_login && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
        exit;
    }else if(is_user_logged_in() && $is_msf_login){
        wp_redirect(home_url());
        exit;
    }
}

add_action('init','redirect_login_page');

function login_failed() {
    $login_page  = home_url( MSF_LOGIN_SLUG );
    wp_redirect( $login_page . '?login=failed' );
    exit;
}
add_action( 'wp_login_failed', 'login_failed' );

function verify_username_password( $user, $username, $password ) {
    $login_page  = home_url( MSF_LOGIN_SLUG );

    if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?login=empty" );
        exit;
    }

    // Check with SHA-512 encryption from previous database.
    $user_password = strtoupper(hash('SHA512', $_POST['pwd']));
    $user_info = get_user_by('login', $username);

    if($user_info){
        $key = "old_password";
        $single = true;

        $db_user_password = get_user_meta( $user_info->ID, $key, $single );

        if($user_password === $db_user_password){
            $is_remember = isset($_POST['rememberme']) ? $_POST['rememberme'] : false;

            wp_clear_auth_cookie();
            wp_set_current_user ( $user_info->ID );
            wp_set_auth_cookie  ( $user_info->ID, $is_remember );

            $current_user = wp_get_current_user();
            $role_name = '';

            $redirect_to = home_url();

            if(count($current_user->roles) > 0){
                $role_name = $current_user->roles[0];

                if ( 'admin' === $role_name && 'msf author' === $role_name) {
                    $redirect_to = user_admin_url();
                }
            }

            wp_safe_redirect( $redirect_to );
            exit;
        }
    }

    // If this failed to verify with SHA-512 then continue to normal wordpress login flow.
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);

function logout_page() {
    $login_page  = home_url( MSF_LOGIN_SLUG );
    wp_redirect( home_url() );
    exit;
}
add_action('wp_logout','logout_page');

// Breadcrumbs
function custom_breadcrumbs() {
    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumb';
    $home_title         = 'Home';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        echo '<ol id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
            }

            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
        } else if ( is_single() ) {
            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
            }

            // Get post category info
            $category = get_the_category();

            if(!empty($category)) {
                // Get last category post is in
                $last_category = end(array_values($category));

                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                }
            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
            }

            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';

            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
            } else {
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
            }
        } else if ( is_category() ) {
            // Category page
            echo '<li class="item-current item-cat"><span class="bread-current bread-cat">' . single_cat_title('', false) . '</span></li>';
        } else if ( is_page() ) {
            // Standard page
            if( $post->post_parent ){
                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }

                // Display parent pages
                echo $parents;

                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></li>';
            } else {
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</span></li>';
            }
        } else if ( is_tag() ) {
            // Tag page

            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;

            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><span class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</span></li>';
        } elseif ( is_day() ) {
            // Day archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';

            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';

            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><span class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span></li>';
        } else if ( is_month() ) {
            // Month Archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';

            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><span class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span></li>';
        } else if ( is_year() ) {
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><span class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span></li>';
        } else if ( is_author() ) {
            // Auhor archive

            // Get the author information
            global $author;
            $userdata = get_userdata( $author );

            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><span class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</span></li>';
        } else if ( get_query_var('paged') ) {
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><span class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</span></li>';
        } else if ( is_search() ) {
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><span class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span></li>';
        } elseif ( is_404() ) {
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }

        echo '</ol>';
    }
}

// Check if requires login
function check_if_login_require(){
    // Consider whether this page requires login.
    $is_login_required = get_field('login_required');

    if($is_login_required){
        if( !is_user_logged_in() ){
            $login_page  = home_url( MSF_LOGIN_SLUG );
            wp_redirect($login_page);
            exit;
        }
    }
}

add_action( 'wp', 'check_if_login_require' );

// Disable the auth check for monitoring whether the user is still logged in.
remove_action('admin_enqueue_scripts', 'wp_auth_check_load');

// Set default value of post schedule duration (default published duration is 3 years.)
function publish_post_start_acf_load_field( $field ) {
    date_default_timezone_set('Asia/Bangkok');
    $now = date('m/d/Y g:i:s A');
    $field['default_value'] = $now;

    return $field;
}

add_filter('acf/load_field/name=publish_post_start', 'publish_post_start_acf_load_field');

function publish_post_end_acf_load_field( $field ) {
    date_default_timezone_set('Asia/Bangkok');
    $expired_date = "12/31/" . (date("Y") + 3);
    $field['default_value'] = $expired_date . " 11:59:59 PM";

    return $field;
}

add_filter('acf/load_field/name=publish_post_end', 'publish_post_end_acf_load_field');

function news_year_acf_load_field( $field ) {
    date_default_timezone_set('Asia/Bangkok');
    $field['default_value'] = date("Y");

    return $field;
}

add_filter('acf/load_field/name=year', 'news_year_acf_load_field');

// Enhance the searching to be able to search post with string and meta_query at the same time.
add_action( 'pre_get_posts', function( $q ){
    if( $title = $q->get( '_meta_or_title' ) ){
        add_filter( 'get_meta_sql', function( $sql ) use ( $title ){
            global $wpdb;

            // Only run once:
            static $nr = 0;
            if( 0 != $nr++ ) return $sql;

            // Modified WHERE
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );

            return $sql;
        });
    }
});

add_action( 'pre_get_posts', function( $q ){
    if( $title = $q->get( '_meta_with_title' ) ){
        add_filter( 'get_meta_sql', function( $sql ) use ( $title ){
            global $wpdb;

            // Only run once:
            static $nr = 0;
            if( 0 != $nr++ ) return $sql;

            $search_text = "mt1.meta_key = 'post_title' AND mt1.meta_value LIKE '%" . $title . "%'";
            $sql['where'] = str_replace($search_text, $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title), $sql['where']);

            return $sql;
        });
    }
});

function my_register_fields() {
     include_once( './acf-post-type-selector-master/acf-post-type-selector.php' );
}
add_action( 'acf/include_fields', 'my_register_fields' );