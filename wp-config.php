<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'msf_web');

/** MySQL database username */
define('DB_USER', 'msf_web');

/** MySQL database password */
define('DB_PASSWORD', 'Passw0rd!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/VpBz.mhTf){?kff(1p0E@Qu/Z}R^((y$8_fHwCOq_;`Ahm{(K@iko07+:sMAoR4');
define('SECURE_AUTH_KEY',  ']Ch9r6B* n10H#(F+6`q0cH;#bL_E1%H+P_-g^Syv-6clHajLc6gBTP0O,1at2@b');
define('LOGGED_IN_KEY',    '&lilop%lS8nET>@m+eW..E`C$~dhr1lr50T`i]_04 SgkBXp|t=+}3#&&~C@O V#');
define('NONCE_KEY',        '&35.|,aX1l[E2j#QTaP5|<Uze%EnS;::fILcIQRIJroB0(_ p[U)uQgPb{6>atvI');
define('AUTH_SALT',        'Y^Ig4 DsS+(Z8{C-HfLD;HN#l!z  _N31+28ctWfJ,v=~l6-*{a;z&AOoeq*I>6=');
define('SECURE_AUTH_SALT', '~~[OwB|u10wptH@2ImF?@~mJE3!DAoe)re~FERR]F`:8Tvt3.i~N:sp6|qNswCnp');
define('LOGGED_IN_SALT',   '?2@U1,df^.Ax0d2e-SO[-|Z2DWy9<tH8z<&9KeAj{BaXdi5M+570a3aZ}(D$q7_#');
define('NONCE_SALT',       '3Jq4RIBP6bM=liMg-4Rri(X Q~N,i*(_1Db;sBfop5ZGb6cF8x:1cA+tf2HUiJgW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'msf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_DEBUG_LOG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
